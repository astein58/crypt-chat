package main

import (
	"encoding/json"
	// "fmt"
	"log"

	"golang.org/x/net/websocket"
)

const (
	responseTypeMessage     = 0
	responseTypeNameExist   = 1
	responseTypeBadPassword = 2
	responseTypeNewUser     = 3
	responseTypeInit        = 4
	responseTypeUserQuit    = 5

	clientTypeMessage = 0
	clientTypeClose   = 1
)

type room struct {
	Name         string
	Signature    string
	Participants map[string]*participant_obj
}

func NewRoom(login *loginRequest) *room {
	return &room{Name: login.Room, Signature: login.Room, Participants: make(map[string]*participant_obj)}
}
func (r *room) addParticipant(participant *participant_obj) {
	r.Participants[participant.Name] = participant
}
func (r *room) getParticipant(name string) *participant_obj {
	return r.Participants[name]
}
func (r *room) participantExist(name string) bool {
	if _, ok := r.Participants[name]; ok {
		return true
	}
	return false
}
func (r *room) notifyClientQuit(name string) {
	for _, participant := range r.Participants {
		go participant.notifyClose(name)
	}
}
func (r *room) notifyNewClient(name string) {
	for _, participant := range r.Participants {
		go participant.notifyNewUser(name)
	}
}
func (r *room) getParticipants() []string {
	ret := []string{}
	for _, partObj := range r.Participants {
		ret = append(ret, partObj.EncName)
	}
	return ret
}
func (r *room) sendMessage(msg []byte) {
	for _, partObj := range r.Participants {
		err := partObj.sendMsg(msg)
		if err != nil {
			log.Println("ERROR: sending message to client")
			rmParticipant(r.Name, partObj.Name)
		}
	}
}
func rmParticipant(roomName, name string) {
	rooms[roomName].notifyClientQuit(rooms[roomName].Participants[name].EncName)
	delete(rooms[roomName].Participants, name)
	if len(rooms[roomName].Participants) == 0 {
		delete(rooms, roomName)
	}
}

type participant_obj struct {
	Name    string
	EncName string
	Conn    *websocket.Conn
}

func (p *participant_obj) notifyClose(name string) {
	mapMsg := map[string]interface{}{"Type": responseTypeUserQuit, "Name": name}
	byteMsg, _ := json.Marshal(mapMsg)
	p.Conn.Write(byteMsg)
}
func (p *participant_obj) notifyNewUser(name string) {
	mapMsg := map[string]interface{}{"Type": responseTypeNewUser, "Name": name}
	byteMsg, _ := json.Marshal(mapMsg)
	p.Conn.Write(byteMsg)
}
func (p *participant_obj) sendMsg(msg []byte) error {
	_, err := p.Conn.Write(msg)
	if err != nil {
		return err
	}
	return nil
}

func NewParticipant(name, encName string, conn *websocket.Conn) *participant_obj {
	return &participant_obj{Name: name, EncName: encName, Conn: conn}
}

type loginRequest struct {
	Room    string
	Name    string
	EncName string
}

type socketResponse struct {
	Type         uint8
	Message      string   `json:",omitempty"`
	Writer       string   `json:",omitempty"`
	Participants []string `json:",omitempty"`
}

func NewSocketResponseErr(code uint8, message string) *socketResponse {
	return &socketResponse{Type: code, Message: message}
}
func NewSocketResponseMessage(message string) *socketResponse {
	return &socketResponse{Type: responseTypeMessage, Message: message}
}
func NewSocketResponseInit(r *room) *socketResponse {
	return &socketResponse{Type: responseTypeInit, Participants: r.getParticipants()}
}
func (s *socketResponse) AsJSON() []byte {
	buff, _ := json.Marshal(s)
	return buff
}

type clientMessage struct {
	Type     uint
	Message  string
	EncName  string `json:",omitempty"`
	SingRoom string `json:",omitempty"`
}

func (c *clientMessage) getRoom() string {
	return c.SingRoom
}
