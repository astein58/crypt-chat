# README #

Crypt-chat is a web base interface to chat securely between clients of same room using the correct password.

The server don't save, read or any thing else than transmit the messages as is to every clients of the room.
The password never leave the web browser and is not saved anywhere (no session store or cookie).

Every message is fully encrypted client side, as well as the sender.

The all application runs in one single page with (HTML5/CSS3/JS/AJAX - JQUERY).
Every messages pass through only one web-socket connexion.

### What is this repository for? ###

* Keep your conversation fully private with no risks for your secrets.
* Version: 1.0

### How do I get set up? ###

* Setup constant in the "main.go" file to specify your listen port and if you want to use TLS setup also the certificate and key
* The sofware has no dependences expect to build the binary

### Who do I talk to? ###

* Alexandre Stein at INTERLAB (interlab-net.com)
* alexandre_stein@interlab-net.com
