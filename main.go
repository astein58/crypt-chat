package main

import (
	// "fmt"
	"encoding/json"
	"log"
	"net/http"
	"text/template"
	"time"

	"golang.org/x/net/websocket"

	"github.com/gorilla/mux"
)

const (
	tls = false

	bind      = ":8080"
	key_path  = "/home/alexandre/parions-geek/ssl/server.key"
	cert_path = "/home/alexandre/parions-geek/ssl/server.crt"
)

var (
	tmpl  *template.Template
	rooms = make(map[string]*room)
)

func main() {
	if tmp_tmpl, tmpl_err := template.ParseFiles("main.html"); tmpl_err != nil {
		log.Fatal(tmpl_err)
	} else {
		tmpl = tmp_tmpl
	}

	mux := mux.NewRouter()
	mux.HandleFunc("/", index)
	mux.Handle("/init", websocket.Handler(initHandler))
	// Files
	mux.PathPrefix("/files").Handler(http.FileServer(http.Dir("./")))

	var err error
	// If TLS is specify
	if tls {
		err = http.ListenAndServeTLS(bind, cert_path, key_path, mux)
	} else {
		err = http.ListenAndServe(bind, mux)
	}
	if err != nil {
		log.Println("ERROR: setting up server:", err)
	}
}

func index(w http.ResponseWriter, req *http.Request) {
	// The "/" pattern matches everything, so we need to check
	// that we're at the root here.
	if req.URL.Path != "/" {
		http.NotFound(w, req)
		return
	}
	tmpl.ExecuteTemplate(w, "main", nil)
}

func initHandler(ws *websocket.Conn) {
	ws.SetDeadline(time.Now().Add(time.Minute * 20))
	defer ws.Close()

	buf := make([]byte, 1024)
	n, read_err := ws.Read(buf)
	if read_err != nil {
		log.Println("ERROR: ", read_err)
		return
	} else {
		// Clean the rest of the buffer
		buf = buf[0:n]
	}

	login := &loginRequest{}
	unmarshal_err := json.Unmarshal(buf, login)
	if unmarshal_err != nil {
		log.Println("ERROR: ", unmarshal_err)
		return
	}

	// If the room exist
	if room, ok := rooms[login.Room]; ok {
		// If the existing room has the same signature
		if room.Signature == login.Room {
			// If the participant do not already exist
			if !room.participantExist(login.Name) {
				addUserToRoom(login, ws)
				// The user exist, this is an error
			} else {
				resp := NewSocketResponseErr(responseTypeNameExist, "The nick name is already in use")
				ws.Write(resp.AsJSON())
				ws.Close()
			}
			// The room exist with a different signature, this is ab other error
		} else {
			time.Sleep(time.Second * 1)
			resp := NewSocketResponseErr(responseTypeBadPassword, "The password is incorrect")
			ws.Write(resp.AsJSON())
			ws.Close()
		}
		// If the room do not exist
	} else {
		buildNewRoom(login, ws)
	}
	for {
		msg := make([]byte, 4096)
		n, err := ws.Read(msg)
		if err != nil {
			log.Println("ERROR: read socket has issue:", err.Error())
			break
		} else {
			msg = msg[0:n]
		}

		mes := &clientMessage{}
		jsonErr := json.Unmarshal(msg, mes)
		if jsonErr != nil {
			log.Println("ERROR: Client message to object issue:", jsonErr)
			break
		}

		if mes.Type == clientTypeClose {
			log.Println("INFO: user closed the connexion")
			break
		}
		if mes.Type == clientTypeMessage {
			msg, err := json.Marshal(mes)
			if err != nil {
				log.Println("ERROR: message to JSON:", err)
				break
			}
			sendMessageInTheRoom(msg, mes.getRoom())
			continue
		}
	}
	rmParticipant(login.Room, login.Name)
}

func buildNewRoom(login *loginRequest, ws *websocket.Conn) {
	// Build the participant
	tmp_participant := NewParticipant(login.Name, login.EncName, ws)
	// Build the room
	tmp_room := NewRoom(login)
	// Add the participant to the room
	tmp_room.addParticipant(tmp_participant)
	// Add the room to the maintained listing of active rooms
	rooms[login.Room] = tmp_room
	// Return to the imitator that the login is success full
	resp := NewSocketResponseInit(tmp_room)
	ws.Write(resp.AsJSON())

	// Start new go routine to clean forgotten rooms
	go vanishedEmptyRooms()
}

func addUserToRoom(login *loginRequest, ws *websocket.Conn) {
	// Build the participant
	tmp_participant := NewParticipant(login.Name, login.EncName, ws)
	// Get the room
	tmp_room := rooms[login.Room]
	// Add the participant to the room
	tmp_room.notifyNewClient(login.EncName)
	tmp_room.addParticipant(tmp_participant)
	// Return to the imitator that the login is success full
	resp := NewSocketResponseInit(tmp_room)
	ws.Write(resp.AsJSON())
}

func sendMessageInTheRoom(msg []byte, room string) {
	rooms[room].sendMessage(msg)
}

func vanishedEmptyRooms() {
	for key, room := range rooms {
		if len(room.Participants) == 0 {
			delete(rooms, key)
		}
	}
}
