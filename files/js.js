(function(){
	// Save the websocket extention (secure or not)
	var wsProto = "";
	// If HTTP -> WS or HTTPS -> WSS
	if (window.location.protocol === "http:") {wsProto = "ws://"} else {wsProto = "wss://"};

	window.sharedElements = {};

	$("body").on("click", "#login button", function(e){
		var now = new Date().getTime(),
			socket = new WebSocket(wsProto + window.location.hostname + ":" + window.location.port + "/init"),
			jsonLongin = "",
			login = {
				room: CryptoJS.HmacSHA3($("#login>div.input>input[name=room]").val(), $("#login>div.input>input[name=pass]").val()).toString(CryptoJS.enc.Base64),
				name: CryptoJS.HmacSHA3($("#login>div.input>input[name=name]").val(), $("#login>div.input>input[name=pass]").val()).toString(CryptoJS.enc.Base64),
				encName: encrypt($("#login>div.input>input[name=name]").val(), $("#login>div.input>input[name=pass]").val())
			};
		window.sharedElements.connexion = socket;
		window.sharedElements.name = $("#login>div.input>input[name=name]").val();
		window.sharedElements.nameSign = login.nameSign;
		window.sharedElements.roomSign = login.room;
		window.sharedElements.passWord = $("#login>div.input>input[name=pass]").val();
		jsonLongin = JSON.stringify(login);

		socket.onopen = function(ev){
			this.send(jsonLongin);
		};
		socket.onmessage = function(ev){
			var rep = $.parseJSON(ev.data);
			if (rep.Type === 0) {
				var name = decrypt(rep.EncName, window.sharedElements.passWord),
					msg = decrypt(rep.Message, window.sharedElements.passWord),
					color = $("#participants #" + name + " .color").html(),
					self = "",
					senderLab =  "<div><b>" + name + "</b> said:</div>";

				if (window.sharedElements.name === name) {
					self = " user";
					senderLab = "";
				};
				
				$("#chat>#conversation").append("<div class=\"message" + self + "\">" + senderLab + "<div class=\"msg\" style=\"background-color:" + color + "\">" + msg + "</div></div>");
			// If the nick name is already used
			} else if (rep.Type === 1) {
				$("#errorModal .modal-title").html("Nick Name Error");
				$("#errorModal .modal-body>p").html(rep.Message);
				$("#errorModal").modal("show");
			// If the password is incorrect
			} else if (rep.Type === 2) {
				$("#errorModal .modal-title").html("Password Error");
				$("#errorModal .modal-body>p").html(rep.Message);
				$("#errorModal").modal("show");
			// If new user connected  
			} else if (rep.Type === 3) {
				var name = decrypt(rep.Name, window.sharedElements.passWord),
					col = getRandomColor();
				$("#chat>#participants>ul").append("<li id=\"" + name + "\" class=\"list-group-item\"><span class=\"color cacher\">" + col + "</span>" + name + "</li>");
				$("#chat>#conversation").append("<div class=\"userInfo\"><div><b style=\"background-color:" + col + "\">" + name + "</b> joined the room</div></div>");
				$("#chat>#participants>ul>li#" + name).css("background-color", col);
			// If user quited
			} else if (rep.Type === 5) {
				var name = decrypt(rep.Name, window.sharedElements.passWord),
					col = $("#participants #" + name + " .color").html();
				$("#chat>#conversation").append("<div class=\"userInfo\"><div><b style=\"background-color:" + col + "\">" + name + "</b> exited the room</div></div>");
				$("#chat>#participants>ul>li#" + name).remove();
			// If the user just connected
			} else if (rep.Type === 4) {
				for (var i = 0; i < rep.Participants.length; i++) {
					var name = decrypt(rep.Participants[i], window.sharedElements.passWord),
						col = getRandomColor();
					$("#chat>#participants>ul").append("<li id=\"" + name + "\" class=\"list-group-item\"><span class=\"color cacher\">" + col + "</span>" + name + "</li>");
					$("#chat>#participants>ul>li#" + name).css("background-color", col);
				};
				toggleInterface();
			};
			$("#chat>#conversation").scrollTop($("#chat>#conversation")[0].scrollHeight);
		};
		socket.onclose = function(ev){
			console.info("Socket has been closed!");
			toggleInterface();
		};
		socket.onerror = function (ev) {
			console.error(ev);
			socket.close();
		};
	});

	function toggleInterface() {
		$("body>div.interface").toggleClass("cacher");
		$("#chat>#sendArea>#sendButtonConntainer").css("line-height", $("#chat>#sendArea>#sendButtonConntainer").height() + "px")
	};

	function getRandomColor() {
		return "rgba("
			+((255)*Math.random()|0).toString(10)+","
			+((255)*Math.random()|0).toString(10)+","
			+((255)*Math.random()|0).toString(10)+",0.6)";
	};
	
	function buildMessage(message, type){
		// If no type is specified the default is regular message
		var request = {};
		request.type = (typeof type === "undefined") ? 0 : type;
		request.message = message;
		return JSON.stringify(request);
	};

	$("#sendButtonConntainer").click(sendMessage);
	$("#sendArea>textarea").keyup(function (e){
		if (e.keyCode == 13){
			sendMessage();
		};
    });

	function sendMessage(){
		var txt = $("#sendArea>textarea").val();
			encTxt = encrypt(txt, window.sharedElements.passWord),
			encName = encrypt(window.sharedElements.name, window.sharedElements.passWord)
			msgObj = {
				Type: 0,
				Message: encTxt,
				EncName: encName,
				SingRoom: window.sharedElements.roomSign
			},
			msgJSON = JSON.stringify(msgObj);
		$("#sendArea>textarea").val("");
		// console.log(msgJSON);
		// console.log(msgObj);
		// console.log(txt);
		// console.log(encTxt);
		// console.log(decrypt(encTxt, window.sharedElements.passWord));
		window.sharedElements.connexion.send(msgJSON);
	};

	window.onbeforeunload = function(e){
		var req = buildMessage("", 1);
		window.sharedElements.connexion.send(req);
		window.sharedElements.connexion.close();
	};
})()